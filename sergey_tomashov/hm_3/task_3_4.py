# Напишите программу которая удаляет пробел в начале, в конце строки


str_given = ' gshdgshdgtee d '
str_list = str_given.split()
str_final = ' '.join(str_list)

# One-line solution
# str_final = ' '.join(str_given.split())

print(f'First symbol: {str_final[0]}\t '
      f'Last symbol: {str_final[len(str_final) - 1]}')
