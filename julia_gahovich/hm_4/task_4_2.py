"""Дан список: [Ivan, Ivanou], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
"""

list_1 = list(['Ivan', 'Ivanou'])
str_1 = 'Minsk'
str_2 = 'Belarus'

list_1 = ' '.join(list_1)
print('Привет, ' + list_1 + '! Добро пожаловать в ' + str_1 + ' ' + str_2)
